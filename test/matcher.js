import {expect} from 'chai';
import matcher from '../src/matcher';

describe('Feature: Pattern matcher', () => {
    describe('Scenario: Matcher invoked as function with no args', () => {
        let matcherEvaluator;
        let results;

        before('when matcher invoked', () => {
            matcherEvaluator = matcher();
        });
        before('when matcher evaluated', () => {
            results = matcherEvaluator();
        });
        it('then should evaluate to undefined', () => {
            expect(results).to.equal(undefined);
        });
    });

    describe('Scenario: Matcher provided default resolution', () => {
        const expectedDefault = Symbol('expected default');
        let matcherEvaluator;
        let defaultEvaluationHandler;
        let results;

        before('given default evaluation handler', () => {
            defaultEvaluationHandler = () => expectedDefault;
        });

        before('when matcher invoked with default handler', () => {
            matcherEvaluator = matcher([], defaultEvaluationHandler);
        });
        before('when matcher evaluated', () => {
            results = matcherEvaluator();
        });

        it('then should evaluate to expected default value', () => {
            expect(results).to.equal(expectedDefault);
        });
    });

    describe('Scenario: Matcher provided default resolution against value', () => {
        const expectedDefault = Symbol('expected default');
        const expectedTestValue = Symbol('expected value');
        let matcherEvaluator;
        let defaultEvaluationHandler;
        let results;

        before('given default evaluation handler', () => {
            defaultEvaluationHandler = (x) => {
                if (x === expectedTestValue) {
                    return expectedDefault;
                }
                return Symbol('incorrect result');
            };
        });

        before('when matcher invoked with default handler', () => {
            matcherEvaluator = matcher([], defaultEvaluationHandler);
        });
        before('when value evaluated by matcher', () => {
            results = matcherEvaluator(expectedTestValue);
        });

        it('then should evaluate to expected default value', () => {
            expect(results).to.equal(expectedDefault);
        });
    });

    describe('Scenario: Matcher provided matcher rules', () => {
        const passingTestValue = Symbol('test value');
        const expectedResult = Symbol('expected result');
        const rules = [];
        let matcherEvaluator;
        let results;

        before('given matching rule', () => {
            rules.push({
                when: x => x === passingTestValue,
                then: (value) => {
                    if (value === passingTestValue) {
                        return expectedResult;
                    }
                    return Symbol('incorrect result');
                }
            });
        });
        before('given non-matching rule', () => {
            rules.push({
                when: () => false,
                then: () => Symbol('not a match')
            });
        });

        before('when matcher invoked with rules', () => {
            matcherEvaluator = matcher(rules);
        });
        before('when value evaluated by matcher', () => {
            results = matcherEvaluator(passingTestValue);
        });

        it('then should evaluate to expected default value', () => {
            expect(results).to.equal(expectedResult);
        });
    });

    describe('Scenario: Matcher evaluated against multiple args', () => {
        const testValue1 = Symbol('test value 1');
        const testValue2 = Symbol('test value 2');
        const passingTestValue = [testValue1, testValue2];
        const expectedResult = Symbol('expected result');
        const rules = [];
        let results;

        before('given matching rule', () => {
            rules.push({
                when: (...x) => x[0] === testValue1 && x[1] === testValue2,
                then: () => expectedResult
            });
        });

        before('when value evaluated by matcher', () => {
            const matcherEvaluator = matcher(rules);

            results = matcherEvaluator(...passingTestValue);
        });

        it('then should evaluate to expected default value', () => {
            expect(results).to.equal(expectedResult);
        });
    });
});
