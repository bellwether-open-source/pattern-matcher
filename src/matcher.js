export default function (matchers = [], defaultValueHandler = () => undefined) {
    return (...values) => {
        const matcher = matchers.find(({when}) => when(...values));

        if (matcher) {
            return matcher.then(...values);
        }
        return defaultValueHandler(...values);
    };
}
