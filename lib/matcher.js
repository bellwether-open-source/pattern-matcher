"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    var matchers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var defaultValueHandler = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {
        return undefined;
    };

    return function () {
        for (var _len = arguments.length, values = Array(_len), _key = 0; _key < _len; _key++) {
            values[_key] = arguments[_key];
        }

        var matcher = matchers.find(function (_ref) {
            var when = _ref.when;
            return when.apply(undefined, values);
        });

        if (matcher) {
            return matcher.then.apply(matcher, values);
        }
        return defaultValueHandler.apply(undefined, values);
    };
};