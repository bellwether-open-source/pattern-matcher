# pattern-matcher

[![Test status](https://ci.appveyor.com/api/projects/status/di9834n1ix6miryt?svg=true)](https://ci.appveyor.com/project/SpaceMacGyver/pattern-matcher)

The **pattern-matcher** project attempts to give functional developers a simple-to-use analogue to similar mechanisms found in other languages (e.g. [Scala](http://docs.scala-lang.org/tutorials/tour/pattern-matching.html)).  While the mechanism provided could alternatively be written as a series of `if`-`else if`-`else` statements, many developers may find pattern matching to be a viable alternative.

## Usage

```javascript
const patternMatcher = require('pattern-matcher');
const evaluator = patternMatcher([rules], defaultHandler);
const evaluatedResults = evaluator(value);  // also accepts variable arguments
```
* `evaluator` is the matching handler from the provided rules, or if no match is found then `defaultHandler` is returned
  - if no `defaultHandler` is specified, a built-in `defaultHandler` exists that will always evaluate to `undefined` when run
* `[rules]` is an array of ordered objects describing patterns against which to test, and the resulting handler to return when the first available match is found.  Each rule expects the following properties to be provided:
  - `.when` is a function that, given input, returns a truthy evaluation of that value
  - `.then` is a function that performs a transformation against the given input
* `defaultHandler` is a function that, if no matching rules are found, performs a transformation against the given input
* `evaluator(...values)` will begin evaluation of the provided when/then clauses as follows:
  - For each rule in the order provided, `when` clauses will be evaluated against the provided `values`.
  - Once a matching `when` clause is identified, its corresponding `then` clause is evaluated against `values` and its results returned.
  - Should no matching clause be identified, the return value will be the results of the `defaultHandler` evaluation of `values`.

## Examples

### Fizz Buzz

The rules for Fizz Buzz, as described at [wiki.c2.com](http://wiki.c2.com/?FizzBuzzTest), are as follows:

* For every item in an array from 1 to 100:
  - If the item is a multiple of 3, echo "Fizz"
  - If the item is a multiple of 5, echo "Buzz"
  - If the item is a multiple of both 3 and 5, echo "FizzBuzz"
  - Else, echo the item
* The above results should be displayed on their own lines (newline-separated)

Applying the pattern matcher to this problem could be achieved as shown below:

```javascript
const patternMatcher = require('pattern-matcher');

const arrayFrom1To100 = Array(100).fill().map((x, index) => index + 1);

function isMod3(x) {
  return x % 3 === 0;
}
function isMod5(x) {
  return x % 5 === 0;
}
function isMod3AndMod5(x) {
  return isMod3(x) && isMod5(x);
}

const fizzBuzz = patternMatcher([
  {when: isMod3AndMod5, then: () => 'FizzBuzz'},
  {when: isMod3, then: () => 'Fizz'},
  {when: isMod5, then: () => 'Buzz'}
], x => x);

const fizzBuzzOutput = arrayFrom1To100.map(fizzBuzz).join('\n');
console.log(fizzBuzzOutput); // should render expected FizzBuzz results
```

Because the matching process completes on the first match, it is necessary to provide the "FizzBuzz" test first.  The default identity handler (`x => x`) ensures that a non-match still presents the original input value.

### Factory

Consider a serialized array of objects being imported, with the following structure:

```javascript
const importedCars = [
    {make: 'Kuruma', model: 'Type Z', vin: '123456'},
    {make: 'Kuruma', model: 'Type L', vin: '835623'},
    {make: 'Autocar', model: 'Micro', vin: '838k23'},
    {make: 'Ziibra', model: 'Itsybit', vin: '836834'}
];
```

Also suppose that we have the following polymorphic types:

```javascript
class Car {
    constructor(properties) {
        this.make = properties.make;
        this.model = properties.model;
        this.vin = properties.vin;
    }
}

class Kei extends Car {
}

class Sedan extends Car {
}
```

... and we know the following about cars:

* A vin containing the letter 'k' is always a Kei car
* Every car made by Ziibra is a Kei car
* The Kuruma Type L is a Sedan

We might use a pattern matcher to instantiate the appropriate types based on the above rules.  Our processed data should result in:

* `importedCars[0]` (Kuruma Type Z) should be a `Car` (default type)
* `importedCars[1]` (Kuruma Type L) should be a `Sedan` (match on make and model comparison)
* `importedCars[2]` (Autocar Micro) should be a `Kei` (match on vin comparison)
* `importedCars[3]` (Ziibra Itsybit) should be a `Kei` (match on make)

The pattern matcher might look like the following:

```javascript
const patternMatcher = require('pattern-matcher');
const carDetailsMapper = patternMatcher([
    {
        when: details => /k/.test(details.vin),
        then: details => new Kei(details)
    },
    {
        when: details => details.make === 'Ziibra',
        then: details => new Kei(details)
    },
    {
        when: details => details.make === 'Kuruma' && details.model === 'Type L',
        then: details => new Sedan(details)
    }
], (details) => new Car(details));

const cars = importedCars.map(carDetailsMapper);

console.log(cars); /* results in the following output:
[ Car { make: 'Kuruma', model: 'Type Z', vin: '123456' },
  Sedan { make: 'Kuruma', model: 'Type L', vin: '835623' },
  Kei { make: 'Autocar', model: 'Micro', vin: '838k23' },
  Kei { make: 'Ziibra', model: 'Itsybit', vin: '836834' } ]
*/
```
